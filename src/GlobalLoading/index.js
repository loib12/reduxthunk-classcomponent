import { withStyles } from '@material-ui/styles';
import React, { Component } from 'react';
import styles from './styles'
import PropTypes from 'prop-types'
import LoadingIcon from '../assets/images/loading.gif'

class GlobalLoading extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.globalLoading}>
                <img src={LoadingIcon} alt='Loading' className={classes.icon} />
            </div>
        );
    }
}

GlobalLoading.propTypes = {
    classes: PropTypes.object
}

export default withStyles(styles)(GlobalLoading);