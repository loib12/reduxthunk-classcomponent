import * as taskApis from '../apis/task'
import * as taskConstants from '../constants/task'

export const fetchListTask = () => {
    return {
        type: taskConstants.FETCH_TASK
    }
};

export const fetchListTaskSUCCESS = (data) => {
    return {
        type: taskConstants.FETCH_TASK_SUCCESS,
        payload: {
            data
        }
    }
};

export const fetchListTaskFAILED = (error) => {
    return {
        type: taskConstants.FETCH_TASK_FAILED,
        payload: {
            error
        }
    }
};


// export const fetchListTaskRequest = () => {
//     return dispatch => {
//         dispatch(fetchListTask())
//         taskApis.getList().then(res => {
//             const { data } = res
//             dispatch(fetchListTaskSUCCESS(data))
//         })
//         .catch(error => {
//             dispatch(fetchListTaskFAILED(error))
//         })
//     }
// }