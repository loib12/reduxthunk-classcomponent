import { fork, take, call, put } from 'redux-saga/effects'
import * as taskTypes from '../constants/task'
import { getList } from '../apis/task'
import { STATUS_CODE } from '../constants'
import { fetchListTaskFAILED, fetchListTaskSUCCESS } from '../actions/task'

function* watchFetchListTaskAction() {
    while(true) {
        yield take(taskTypes.FETCH_TASK) // block cho den khi xong, đứng đợi action FETCH_TASK
        const resp = yield call(getList) // block cho den khi xong
        const { status, data } = resp
        if(status === STATUS_CODE.SUCCESS) {
            yield put(fetchListTaskSUCCESS(data))
        } else {
            yield put(fetchListTaskFAILED(data))
        }
    }
} 

function* watchCreateTaskAction() {
    console.log('watching create task action')
}


function* rootSaga() {
    yield fork(watchFetchListTaskAction)
    yield fork(watchCreateTaskAction)
}

export default rootSaga