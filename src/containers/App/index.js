import { Button, withStyles } from '@material-ui/core';
import React, { Component } from 'react'
import Styles from './styles';
import { ThemeProvider } from '@material-ui/styles'
import TaskBoards from '../TaskBoards';
import theme from '../../commons/theme'
import { Provider } from 'react-redux'
import configureStore from '../../redux/configStore'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GlobalLoading from '../../GlobalLoading';

const store = configureStore()

class App extends Component {
  
  render() {
    
    const { classes } = this.props
    console.log(classes)
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <ToastContainer />
          <GlobalLoading />
          <TaskBoards />
        </ThemeProvider>
      </Provider>
    )
  }
}


export default withStyles(Styles)(App);
