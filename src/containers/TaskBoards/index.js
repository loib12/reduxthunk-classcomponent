import { withStyles } from "@material-ui/styles";
import React, { Component } from "react";
import Styles from "./styles";
import { Button } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { Grid } from "@material-ui/core";
import { STATUSES } from "../../constants";
import TaskList from "../../components/TaskLists";
import TaskForm from "../../components/TaskForm";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import * as taskActions from '../../actions/task'
import PropTypes from 'prop-types'


class TaskBoard extends Component {
  state = {
    open: false,
  };

  renderBoard() {
    const { listTask } = this.props
    let xhtml = null;
    xhtml = (
      <Grid container spacing={2}>
        {STATUSES.map((status, index) => {
          const taskFiltered = listTask.filter(
            (task) => task.status === status.value
          );
          return <TaskList tasks={taskFiltered} status={status} key={index} />;
        })}
      </Grid>
    );
    return xhtml;
  }

  // componentDidMount() {
  //   const { taskActionCreator } = this.props
  //   const { fetchListTask } = taskActionCreator
  //   fetchListTask()
  // }

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  openForm = () => {
    this.setState({
      open: true,
    });
  };

  renderForm() {
    const { open } = this.state;
    let xhtml = null;
    xhtml = <TaskForm open={open} onClose={this.handleClose} />;
    return xhtml;
  }  

  loadData = () => {
    const { taskActionCreator } = this.props
    const { fetchListTask } = taskActionCreator
    fetchListTask()
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.taskBoard}>
        <Button variant="contained" color="secondary" onClick={this.loadData} style={{marginRight: 20}}>
          Load Data
        </Button>
        <Button variant="contained" color="secondary" onClick={this.openForm}>
          <AddIcon />
        </Button>
        
        {this.renderBoard()}
        {this.renderForm()}
      </div>
    );
  }
}

TaskBoard.prototypes = {
  classes: PropTypes.object,
  taskActionCreator: PropTypes.shape({
    fetchListTask: PropTypes.func,
  }),
  listTask: PropTypes.array
}

const mapStateToProps = state => {
  return {
    listTask: state.taskReducer.listTask
  }
}
const mapDispatchToProps = dispatch => {
  return {
    taskActionCreator: bindActionCreators(taskActions, dispatch)
  }
}

export default withStyles(Styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TaskBoard)
);
