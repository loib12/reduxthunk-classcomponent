import { combineReducers } from 'redux'
import reducer from './task'

const rootReducer = combineReducers({
    taskReducer: reducer
})

export default rootReducer