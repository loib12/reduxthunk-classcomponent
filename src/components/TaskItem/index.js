import { withStyles } from "@material-ui/styles";
import React, { Component } from "react";
import styles from "./styles";
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
} from "@material-ui/core";
import { Fab } from "@material-ui/core";
import { Edit, Delete } from "@material-ui/icons";

//import Icon from '@mui/material/Icon'

class TaskItem extends Component {
  render() {
    const { classes, task, status } = this.props;
    const { title, description } = task;
    return (
      <Card key={task.id} className={classes.card}>
        <CardContent>
          <Grid container justify="space-between">
            <Grid item md={8}>
              <Typography component="h2">{title}</Typography>
            </Grid>
            <Grid item md={4}>
              {status.label}
            </Grid>
          </Grid>
          <p>{description}</p>
        </CardContent>
        <CardActions className={classes.cardActions}>
          <Fab color="primary" aria-label="Edit" className={classes.fab} size="small">
            <Edit />
          </Fab>
          <Fab color="secondary" aria-label="Delete" className={classes.fab} size="small">
            <Delete />
          </Fab>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles)(TaskItem);
