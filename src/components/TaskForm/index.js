import { withStyles } from '@material-ui/styles';
import React, { Component } from 'react';
import styles from './styles';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Button } from "@material-ui/core";
import TextField from '@mui/material/TextField';

class TaskForm extends Component {
    render() {
        const { open, classes, onClose } = this.props
        return (
            <div>
                <Dialog open={open} onClick={onClose}>
                    <DialogTitle>Add new todo</DialogTitle>
                    <DialogContent>
                        <TextField
                        required
                        id="outlined-required"
                        className={classes.TextField}
                        label="Name"
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Multiline"
                        multiline
                        maxRows={4}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose}>Cancel</Button>
                        <Button onClick={onClose}>Ok</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(TaskForm);