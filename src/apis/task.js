import axiosServices from "../commons/axiosServices";
import { API_ENDPOINT } from '../constants'

const url = 'tasks'

export const getList = () => {
    return axiosServices.get(`${API_ENDPOINT}/${url}`)
}